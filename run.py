#imports all the things i need
import gi


gi.require_version("Gtk", "3.0")
gi.require_version("WebKit2", "4.0")
from gi.repository import Gtk, WebKit2



class Header(Gtk.Window):
    def __init__(self):
        # sets a title
        super().__init__(title="MR:HB")
        # Creates a window with a border
        self.win = Gtk.Window()
        self.set_border_width(10)
        self.set_default_size(1200, 500)

        

        # main conatiner
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)

        # grid
        self.grid = Gtk.Grid()
        vbox.pack_start(self.grid, False, False, 0)

        # sets a custom header with title of a header
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "MirrorReese: Browser"
        self.set_titlebar(hb)
            

        # Craetes a serach bar, sb means search bar
        self.sb = Gtk.Entry()
        # sets text as url
        self.sb.set_text("https://duck.com")
        # expands the search bar
        self.sb.set_hexpand(True)
        self.grid.add(self.sb)
        
        # search buttton
        sb_btn = Gtk.Button(label="Search")
        # passes through a command to change whats seen on self.display
        sb_btn.connect('clicked', self.on_button)
        self.grid.attach_next_to(sb_btn, self.sb, Gtk.PositionType.RIGHT, 1, 1)
        
        # displays the url of self.sb
        self.display = WebKit2.WebView() 
        self.display.load_uri("https://duck.com") 
        # adds ability to scroll
        scrolla = Gtk.ScrolledWindow()
        scrolla.add(self.display)
        vbox.pack_start(scrolla, True, True, 0)

        
    # command updates url
    def on_button(self, event):
            add = self.sb.get_text()
            self.display.load_uri(add)


# loads up the window
win1 = Header()
win1.show_all()
Gtk.main()

